import { Component } from '@angular/core';
import { BookApiService } from './book-api.service';
import { BookVolumeInfo } from './models/book-volume-info';
import { BookItems } from './models/book-items';
import { BookSearchBar } from './models/book-search-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  booksVolumeInfo: BookVolumeInfo[] = [];
  title = 'mybook-app';

  bookDetailVolumeInfo: BookVolumeInfo = {
    authors: [],
    description: '',
    imageLinks: {smallThumbnail: '', thumbnail: ''},
    publishedDate: '',
    publisher: '',
    subtitle: '',
    title: ''
  }

  constructor(private bookApiService: BookApiService){}
  ngOnInit(){
    this.bookApiService.showBooks()
    .subscribe((books: BookItems[]) => {
      for(let bookItems of books){
        // this.booksVolumeInfo = [...this.booksVolumeInfo, bookItems.volumeInfo];
        this.booksVolumeInfo.push(bookItems.volumeInfo);
        // console.log(this.booksVolumeInfo);
      }
    });
    console.log(this.booksVolumeInfo);
  }
  searchBooks(bookSearchBar: BookSearchBar){
    this.booksVolumeInfo = [];
    if(bookSearchBar.bookName && bookSearchBar.authorName){
      this.bookApiService.getBookByNameAndAuthor(bookSearchBar.bookName, bookSearchBar.authorName)
      .subscribe((books: BookItems[]) => {
        for(let bookItems of books){
          this.booksVolumeInfo.push(bookItems.volumeInfo);
        }
      });
    }
    else if(bookSearchBar.bookName){
      this.booksVolumeInfo = [];
      this.bookApiService.getBookByName(bookSearchBar.bookName)
      .subscribe((books: BookItems[]) => {
        for(let bookItems of books){
          this.booksVolumeInfo.push(bookItems.volumeInfo);
        }
      });
    }
    else if(bookSearchBar.authorName){
      this.booksVolumeInfo = [];
      this.bookApiService.getBookByAuthor(bookSearchBar.authorName)
      .subscribe((books: BookItems[]) => {
        for(let bookItems of books){
          this.booksVolumeInfo.push(bookItems.volumeInfo);
        }
      });
    }
    console.log(this.booksVolumeInfo);
  }

  bookDetailSection: boolean = false;
  searchAndBookListSection: boolean = true;

  bookDetails(booksVolumeInfo: BookVolumeInfo){
    console.log(booksVolumeInfo);
    this.bookDetailVolumeInfo = booksVolumeInfo;
    this.bookDetailSection = true;
    this.searchAndBookListSection = false;
  }

  searchAndBookListSectionShow(){
    this.bookDetailSection = false;
    this.searchAndBookListSection = true;
  }

  // ngOnInit(){
  //   this.bookApiService.showBooks()
  //   .subscribe((books) => {
  //     console.log(books);
  //   });
  // }

}
