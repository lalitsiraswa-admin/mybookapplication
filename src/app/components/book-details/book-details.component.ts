import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { BookVolumeInfo } from 'src/app/models/book-volume-info';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {
  @Output()
  showSearchAndBookListSection: EventEmitter<void> = new EventEmitter();

  @Input()
  bookDetailVolumeInfo: BookVolumeInfo = {
    authors: [],
    description: '',
    imageLinks: {smallThumbnail: '', thumbnail: ''},
    publishedDate: '',
    publisher: '',
    subtitle: '',
    title: ''
  };
  constructor() { }
  ngOnInit(): void {
  }
  backToHomePage(){
    this.showSearchAndBookListSection.emit();
  }
}
