import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BookSearchBar } from '../../models/book-search-bar';

@Component({
  selector: 'app-book-search',
  templateUrl: './book-search.component.html',
  styleUrls: ['./book-search.component.css']
})
export class BookSearchComponent implements OnInit {

  constructor() { }

  @Output()
  searchBooksEventEmitter: EventEmitter<BookSearchBar> = new EventEmitter<BookSearchBar>();

  ngOnInit(): void {
  }
  searchBooks(bookName: string, authorName: string){
    let bookSearchBar: BookSearchBar = {
      bookName: bookName,
      authorName: authorName,
    }
    // console.log(bookSearchBar);
    this.searchBooksEventEmitter.emit(bookSearchBar);
  }
}
