import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowBookItemsComponent } from './show-book-items.component';

describe('ShowBookItemsComponent', () => {
  let component: ShowBookItemsComponent;
  let fixture: ComponentFixture<ShowBookItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowBookItemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowBookItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
