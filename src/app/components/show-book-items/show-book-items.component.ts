import { isNull } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BookVolumeInfo } from '../../models/book-volume-info';

@Component({
  selector: 'app-show-book-items',
  templateUrl: './show-book-items.component.html',
  styleUrls: ['./show-book-items.component.css']
})
export class ShowBookItemsComponent implements OnInit {
  @Input()
  bookVolumeInfo: BookVolumeInfo[] = [];
  constructor() { }

  @Output()
  bookDetailEventEmitter: EventEmitter<BookVolumeInfo> = new EventEmitter<BookVolumeInfo>();

  ngOnInit(): void {
  }
  bookVolumeInfoDetails(bookVolumeInfo: BookVolumeInfo){
    this.bookDetailEventEmitter.emit(bookVolumeInfo);
    // console.log(bookVolumeInfo);
  }
}
