import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BookMainObject } from './models/book-main-object';
import { BookItems } from './models/book-items';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookApiService {
  constructor(private http: HttpClient) { }
  showBooks(){
    const url = `https://www.googleapis.com/books/v1/volumes?q=harry+inauthor:inga`;
    return this.http.get<BookMainObject>(url)
    .pipe(
      map((bookMainObject) => bookMainObject.items),
    );
  }
  getBookByName(bookName: string){
    const url = `https://www.googleapis.com/books/v1/volumes?q=${bookName}`;
    return this.http.get<BookMainObject>(url)
    .pipe(
      map((bookMainObject) => bookMainObject.items)
    );
  }
  getBookByAuthor(authorName: string){
    const url = `https://www.googleapis.com/books/v1/volumes?q=inauthor:${authorName}`;
    return this.http.get<BookMainObject>(url)
    .pipe(
      map((bookMainObject) => bookMainObject.items)
    );
  }
  getBookByNameAndAuthor(bookName: string, authorName: string){
    const url = `https://www.googleapis.com/books/v1/volumes?q=${bookName}+inauthor:${authorName}`;
    return this.http.get<BookMainObject>(url)
    .pipe(
      map((bookMainObject) => bookMainObject.items),
    );
  }
}
