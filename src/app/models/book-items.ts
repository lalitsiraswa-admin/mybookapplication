import { BookVolumeInfo } from './book-volume-info';

export type BookItems = {
    kind: string,
    id: string,
    etag: string,
    selfLink: string,
    volumeInfo: BookVolumeInfo
}