import  { BookItems } from './book-items';

export type BookMainObject = {
    kind: string,
    totalItems: number,
    items: BookItems[];
}