export type BookImageLink = {
    smallThumbnail: string,
    thumbnail: string
}