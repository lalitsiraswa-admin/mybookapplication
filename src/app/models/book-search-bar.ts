export type BookSearchBar = {
    bookName: string,
    authorName: string,
}