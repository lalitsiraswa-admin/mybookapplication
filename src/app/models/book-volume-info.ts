import { BookImageLink } from './book-image-link';
export type BookVolumeInfo = {
    title: string,
    subtitle: string,
    authors: string[],
    publisher: string,
    publishedDate: string,
    description: string, 
    imageLinks: BookImageLink
}